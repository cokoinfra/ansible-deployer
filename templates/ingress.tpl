
apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  name: {{app}}-{{app_id}}
  annotations:
    cert-manager.io/cluster-issuer: letsencrypt
    nginx.ingress.kubernetes.io/ssl-redirect: "true"
    nginx.ingress.kubernetes.io/proxy-body-size: 1000m
spec:
  tls:
  - hosts:
{% for host in services_to_deploy %}
    - {{host.url}}
{% endfor %}
    secretName: {{app}}-{{app_id}}-tls-secret
  ingressClassName: nginx
  rules:
{% for host in services_to_deploy %}
  - host: {{host.url}}
    http:
      paths:
      - path: /
        pathType: Prefix
        backend:
          service:
            name: {{host.name}}-{{app}}-{{app_id}}
            port:
              number: {{vars[ host.name + '_port']}}
{% endfor %}