apiVersion: databases.digitalocean.com/v1alpha1
kind: DatabaseUser
metadata:
  name: {{app}}-{{app_id}}-user
spec:
  databaseCluster:
    apiGroup: databases.digitalocean.com
    kind: DatabaseClusterReference
    name: coko-prod-postgres
  username: {{app}}-{{app_id}}-user
---
apiVersion: databases.digitalocean.com/v1alpha1
kind: Database
metadata:
  name: {{app}}-{{app_id}}
spec:
  databaseCluster:
    apiGroup: databases.digitalocean.com
    kind: DatabaseClusterReference
    name: coko-prod-postgres
  name: {{app}}-{{app_id}}
---
apiVersion: batch/v1
kind: Job
metadata:
  name: fix-db-permissions-{{app_id}}
spec:
  template:
    metadata:
      name:  fix-db-permissions-{{app_id}}
    spec:
      containers:
      - image: "postgres:latest"
        name: fix-db-permissions-{{app_id}}
        command: [ "psql", "defaultdb", "-c", "alter database \"$(PGNAME)\" owner to \"$(PGNEWUSER)\";" ]
        env:
          - name: PGHOST
            valueFrom:
              configMapKeyRef:
                name: coko-prod-postgres-connection
                key: host
          - name: PGPORT
            valueFrom:
              configMapKeyRef:
                name: coko-prod-postgres-connection
                key: port
          - name: PGPASSWORD
            valueFrom:
              secretKeyRef:
                name: coko-prod-postgres-default-credentials
                key: password
          - name: PGUSER
            valueFrom:
              secretKeyRef:
                name: coko-prod-postgres-default-credentials
                key: username
          - name: PGNEWUSER
            valueFrom:
              secretKeyRef:
                name: {{app}}-{{app_id}}-user-credentials
                key: username
          - name: PGNAME
            value: {{app}}-{{app_id}}
      restartPolicy: OnFailure
---
apiVersion: batch/v1
kind: Job
metadata:
  name: fix-db-extensions-{{app_id}}
spec:
  template:
    metadata:
      name:  fix-db-extensions-{{app_id}}
    spec:
      containers:
      - image: "postgres:latest"
        name: fix-db-extensions-{{app_id}}
        command: [ "psql", "$(PGNAME)", "-c", "CREATE EXTENSION IF NOT EXISTS pgcrypto;CREATE EXTENSION IF NOT EXISTS vector;" ]
        env:
          - name: PGHOST
            valueFrom:
              configMapKeyRef:
                name: coko-prod-postgres-connection
                key: host
          - name: PGPORT
            valueFrom:
              configMapKeyRef:
                name: coko-prod-postgres-connection
                key: port
          - name: PGPASSWORD
            valueFrom:
              secretKeyRef:
                name: coko-prod-postgres-default-credentials
                key: password
          - name: PGUSER
            valueFrom:
              secretKeyRef:
                name: coko-prod-postgres-default-credentials
                key: username
          - name: PGNEWUSER
            valueFrom:
              secretKeyRef:
                name: {{app}}-{{app_id}}-user-credentials
                key: username
          - name: PGNAME
            value: {{app}}-{{app_id}}
      restartPolicy: OnFailure
