# Ansible deployer

This projects holds the files needed to built an ansible docker image that has two scripts available at this time:

* bootstrap.yml - which creates all resources and configurations to deploy.
* teardown.yml - which undoes things.

## Current priorities

Our main goal is to transfer all staging sites to the cluster (Issue #26):

* Label any blockers that arise as ~"transition blocker".
* The Cloud68 team can address blocker issues that require an hour or less of work without waiting for approval from Coko.

## Backlog

The Coko team manages the backlog, with support from Cloud68 when needed:

* Ensure all backlog issues include time estimates.
* Issues labeled ~approved can proceed.

## Usage in an application pipeline

An example gitlab ci configuration for getting a deployment app could look like the following:

```yml
deploy:
  image: $CI_REGISTRY/cokoinfra/ansible-deployer
  services:
    - docker:23-dind
  environment:
    name: review/$CI_COMMIT_REF_NAME
    url: https://$CI_CT_NAMESPACE.$CI_PROJECT_NAME.$CI_COMMIT_REF_NAME.coko.app
    on_stop: teardown test
  variables:
    ANSCOMPOSE_POSTGRES_ALLOW_SELF_SIGNED_CERTIFICATES: true #this is needed on some db setups like DO
    COMPOSE_SERVICES: client,server #comma separated value for each docker-compose service that should be converted into k8s object
    COMPOSE_SERVICES_<service_name>_URL:  #<service name> is each value defined in COMPOSE_SERVICES
    COMPOSE_SERVICES_<service_name>_IMAGE: 
    do_spaces_access_key: $DIGITAL_OCEAN_SPACES_ACCESS_KEY
    do_spaces_secret_key: $DIGITAL_OCEAN_SPACES_SECRET_KEY
  script:
    - kubectl config get-contexts
    - kubectl config use-context <group>/agent:agent-1
    - cp $DEPLOY_COMPOSE_FILE /ansible
    - cd /ansible
    - ANSIBLE_FORCE_COLOR=true ansible-playbook bootstrap.yml -e "compose_file=$DEPLOY_COMPOSE_FILE"

teardown:
  image: $CI_REGISTRY/cokoinfra/ansible-deployer
  services:
    - docker:23-dind
  environment:
    name: review/$CI_COMMIT_REF_NAME
    action: stop
  variables:
    COMPOSE_SERVICES: 
    do_spaces_access_key: $DIGITAL_OCEAN_SPACES_ACCESS_KEY
    do_spaces_secret_key: $DIGITAL_OCEAN_SPACES_SECRET_KEY
  script:
    - kubectl config get-contexts
    - kubectl config use-context kotahi/agent:agent-1
    - cd /ansible
    - ANSIBLE_FORCE_COLOR=true ansible-playbook teardown.yml
```

The ci job uses the cokoinfra/ansible-deployer image that exists in gitlab's container registry. This image contains the code to bootstrap (create) a new app, as well as the code to tear that deployment down. The code in the ansible playbook will create the s3 bucket (in digital ocean spaces) and database for the app as well.

Since the provided container is an extension of the docker-in-docker container (dind), the docker-dind service needs to be added for the job to function correctly.

The job can optionally connect the deployed app to a gitlab review app. This will create a button on merge requests that users can click on to see the deployed app. The connection is done via adding an environment key to the job. Make sure that the url used here matches the url given to the ansible container for the client image. Don't forget to add the `on_stop` key, that points to the ci job that should run if the review app is manually stopped. 

Environment variables that need to be made available to the app containers need to be named as `ANS_COMPOSE_<YOUR_VARIABLE>`.  Eg. If the app uses a variable called `USE_FEATURE_A`, for it to be passed to the deployed container, the `ANS_COMPOSE_USE_FEATURE_A` variable should be passed to the deploy job.

The ansible bootstrap script will read a docker compose file as a template and build the necessary configuration for the app to run. The `COMPOSE_SERVICES` variable will tell the ansible playbook which services from the provided compose file to deploy, in values separated by commas (eg. `client,server`). Then for each service, we provide variables telling ansible which container to use for that service (the value in the provided compose file will be overridden with this value), and what url we want this service to run on. The provided image is most likely an image that has been built during a previous job in the pipeline. Eg. if we just built a preproduction image for the client, and pushed it to the gitlab container registry as `myapp-client-preproduction-<branch_name>`, then we'll tell the deploy job after to use that image via `COMPOSE_SERVICES_CLIENT_IMAGE=myapp-client-preproduction-<branch-name>`.

The two variables `do_spaces_access_key` and `do_spaces_secret_key` are the credentials you need to connect to digital ocean spaces (their service for object storage).

The script first connects to the kubernetes cluster, copies the compose file you want to use as a template into the ansible folder, and finally runs the bootstrap playbook itself.

The teardown script follows the same logic, but does not need `ANS_COMPOSE_` variables or a template compose file to be passed to it.


## Logic behind bootstrap.yml

To get the app running, we first need a few facilities:
0) setting up the right env - a task that's trickier than you might initially plan, the right env consists of variables that define deployment-related configurations, like COMPOSE_SERVICES, variables that define credentials for ansible like do_spaces_access_key, and variables to configure the app itself, which match the vars of the app but are prepended by ANSCOMPOSE_ (e.g ANSCOMPOSE_USE_SANDBOXED_ORCID)
1) S3 bucket - for which we can create/delete as needed through ansible tasks
2) Database - the configuration values of which should be passed to the deployment config files, without ever being exposed or having to be defined. 
3) DatabaseUser - as the cluster might run staging sites, we need to have basic security practices like not having a user access all DBs. To get this done, a fork of the DigitalOcean KuberneteS Operator was written, and installed on the cluster, and can be found at [cokoinfra/doks-operator](https://gitlab.coko.foundation/cokoinfra/doks-operator)
4) kubernetes objects - there's a disconnect happening where devs publish docker-compose files, and meanwhile the cluster only accepts k8s objects (deployments, services, ingress), so kompose.io & was used to perform the translation, where the definition of which compose file to base work on top of is left to the user running the pipeline

### Running only some of the tasks

You can set the bootstrap script to only run specific tasks by using the `--tags` or `--skip-tags` options, acting as a whitelist and a blacklist respectively.

```sh
ansible-playbook bootstrap.yml --tags database # will only run the database task
ansible-playbook bootstrap.yml --skip-tags bucket # will run all tasks except for bucket
```

Valid tags currently are:
* database
* bucket

[Relevant ansible docs](https://docs.ansible.com/ansible/latest/playbook_guide/playbooks_tags.html#selecting-or-skipping-tags-when-you-run-a-playbook)

# Logic behind teardown.yml
Unlike the bootstrap script, the teardown is much simpler, as it simply uses the naming conventions created by executing bootstrap, to find and then delete cluster resources.