FROM smartystreets/version-tools:latest as version-tools

FROM docker:23
RUN apk add python3 py3-pip curl helm
COPY requirements.* /srv/
RUN pip install -r /srv/requirements.txt 
RUN ansible-galaxy install -r /srv/requirements.yml
RUN curl -LO https://storage.googleapis.com/kubernetes-release/release/$(curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt)/bin/linux/amd64/kubectl
RUN curl -LO https://github.com/kubernetes/kompose/releases/download/v1.33.0/kompose-linux-amd64
RUN chmod +x ./kubectl ./kompose-linux-amd64
RUN mv ./kubectl /usr/local/bin
RUN mv ./kompose-linux-amd64 /usr/local/bin/kompose
COPY --from=version-tools / /usr/bin

COPY . /ansible

WORKDIR /ansible